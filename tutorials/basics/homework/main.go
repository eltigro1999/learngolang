package main

import (
	"fmt"
	"math"
	"math/rand"
)

const PassStatus = "pass"
const FailStatus = "fail"

type HealthCheck struct {
	ServiceID uint
	status    string
}

// func main() {
// 	// fmt.Println("First task:")
// 	// first_task()
// 	fmt.Println("Second task:")
// 	fmt.Println(second_task())
// }

func second_task() bool {
	nums := make([]int, 10)
	nums_repeated := make([]int, 10)
	for i := range nums {
		nums[i] = rand.Intn(100)
		nums_repeated[nums[i]]++
		if nums_repeated[nums[i]] > 2 {
			return true
		}
	}
	return false
}

func first_task() {

	fmt.Println("Тут будет выведен идентификатор")
	checks := generateCheck()

	for _, value := range checks {
		if value.status == PassStatus {
			fmt.Println(value.ServiceID)
		}
	}
}

func generateCheck() []HealthCheck {
	var checks = make([]HealthCheck, 5)
	for index := range checks {
		checks[index].ServiceID = uint(index)
		if math.Mod(float64(index), 2) == 0 {
			checks[index].status = PassStatus

		} else {
			checks[index].status = FailStatus
		}
	}
	fmt.Println(checks)

	return checks
}
