package main

import "sync"

func odd(wg *sync.WaitGroup, number chan int) {
	var oddNumbers = [5]int{1, 3, 5, 7, 9}
	for i := range oddNumbers {
		number <- oddNumbers[i]
	}
	wg.Done()
}

func even(wg *sync.WaitGroup, number chan int) {
	var evenNumbers = [5]int{0, 2, 4, 6, 8}
	for i := range evenNumbers {
		number <- evenNumbers[i]
	}
	wg.Done()
}
