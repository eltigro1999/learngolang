package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

func runGoroutins() {
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(2*time.Second))
	defer cancel()
	var (
		contextDone = make(chan bool, 1)
		ctx_done    = false
	)
	wg := &sync.WaitGroup{}
	for i := 0; i < 10000000; i++ {
		if ctx_done {
			break
		}
		select {
		case ctx_done = <-contextDone:
			fmt.Printf("Context is done!")
		default:
			wg.Add(1)
			doSmth(ctx, wg, contextDone)
		}
	}
	wg.Wait()
}

func doSmth(ctx context.Context, wg *sync.WaitGroup, contextDone chan bool) {
	select {
	case <-ctx.Done():
		contextDone <- true
	default:
		fmt.Println("Did something")
	}
	wg.Done()
}
