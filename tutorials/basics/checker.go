package main

import (
	"fmt"
	"strings"
)

type Measurable interface {
	GetMetrics() string
}

type Checkable interface {
	Measurable
	Ping() error
	GetID() string
	Health() bool
}

type Checker struct {
	items []Checkable
}

func (checker Checker) String() string {
	items_str := ""
	for index := range checker.items {
		items_str += checker.items[index].GetID() + " "
	}
	items_str = strings.TrimSuffix(items_str, " ")
	return items_str
}

func (checker *Checker) Add(checkable Checkable) {
	checker.items = append(checker.items, checkable)
}

func (checker *Checker) Check() {
	for index := range checker.items {
		if !checker.items[index].Health() {
			fmt.Printf("Element identifier %v doesn't work\n", index)
		}
	}
}
