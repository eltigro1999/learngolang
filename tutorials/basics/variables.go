package main

import "fmt"

func main() {
	var checker Checker
	client := new(GoMetrClient)
	checker.Add(client)
	client1 := new(GoMetrClient)
	checker.Add(client1)
	client2 := new(GoMetrClient)
	checker.Add(client2)
	checker.Check()
	fmt.Printf("%v", checker)
}
