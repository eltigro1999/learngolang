package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

func newRun() (err error) {
	err = nil
	ctxt, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	wg := &sync.WaitGroup{}

	timerFired := new(bool)
	*timerFired = false

	var iterations int = 100000000

	for i := 0; i < iterations && !*timerFired; i++ {
		wg.Add(1)
		go Multiply(ctxt, 1, 2, wg, timerFired)
	}
	if *timerFired {
		err = fmt.Errorf("Error: time limit exceed. 2 seconds left!\n")
	} else {
		fmt.Println("Functions successfully executed")
	}
	wg.Wait()
	return err
}

func Multiply(ctxt context.Context, a int, b int, wg *sync.WaitGroup, timerFired *bool) {
	_ = a + b
	select {
	case <-ctxt.Done():
		*timerFired = true
	default:
	}
	wg.Done()
}
