package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func CreateList(numbers []int) (*ListNode, bool) {
	var sliceEmpty bool
	if len(numbers) == 1 {
		sliceEmpty = false
		node := ListNode{
			Val:  numbers[0],
			Next: nil,
		}
		return &node, !sliceEmpty
	} else if len(numbers) > 1 {
		sliceEmpty = false
		node := new(ListNode)
		head := new(ListNode)
		for index := range numbers {
			if index == 0 {
				head.Val = numbers[index]
				head.Next = node
			} else if index == len(numbers)-1 {
				node.Val = numbers[index]
				node.Next = nil
			} else {
				node.Val = numbers[index]
				node.Next = new(ListNode)
				node = node.Next
			}
		}
		return head, !sliceEmpty
	}
	sliceEmpty = true
	return &ListNode{}, sliceEmpty
}

func deleteDuplicates(head *ListNode) *ListNode {
	node := head
	for node.Next != nil {
		if node.Next.Val == node.Val {
			node.Next = node.Next.Next
		} else {
			node = node.Next
		}
	}
	return head
}
