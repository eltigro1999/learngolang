package main

type GoMetrClient struct {
	url                  string
	timeLeftUntilTimeout int
}

func (gometr *GoMetrClient) GetMetrics() string {
	return "Metrics"
}

func (gometr *GoMetrClient) Ping() error {
	return nil
}

func (gometr *GoMetrClient) GetID() string {
	return "1"
}

func (gometr *GoMetrClient) Health() bool {
	ID := gometr.getHealth().ServiceID
	return gometr.GetID() == ID

}

func (gometr *GoMetrClient) getHealth() HealthCheck {
	return HealthCheck{
		ServiceID: "1",
	}
}

type HealthCheck struct {
	ServiceID string
	status    string
}
