package main

import (
	"fmt"
	"sort"
)

func main() {
	secondTask()
}

func firstTask() {
	circle := NewCircle()
	fmt.Printf("Circle area: %v\n", circle.Area())
	fmt.Printf("Circle type: %v\n", circle.Type())

	rectangle := NewRectangle()
	fmt.Printf("Rectangle area: %v\n", rectangle.Area())
	fmt.Printf("Rectangle type: %v\n", rectangle.Type())
}

func secondTask() {
	var numbers = []int{3, 1}
	sort.Ints(numbers)
	fmt.Println(numbers)
	head, ok := CreateList(numbers)
	if ok {
		printList(head)
	}
	deleteDuplicates(head)
	printList(head)
}

func printList(head *ListNode) {
	node := head
	for node.Next != nil {
		fmt.Printf("%v ", node.Val)
		node = node.Next
	}
	fmt.Printf("%v\n", node.Val)
}
