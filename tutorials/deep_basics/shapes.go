package main

type Characteristics interface {
	Area() float64
	Type() string
}

type Circle struct {
	_type  string
	pi     float64
	radius float64
}

type Rectangle struct {
	_type  string
	height float64
	width  float64
}

func (rect *Rectangle) Area() float64 {
	return rect.height * rect.width
}

func (circle *Circle) Area() float64 {
	return circle.radius * circle.pi
}

func (rect *Rectangle) Type() string {
	return rect._type
}

func (circle *Circle) Type() string {
	return circle._type
}

func NewCircle() Circle {
	return Circle{
		_type:  "Circle",
		pi:     3.14,
		radius: 3.,
	}
}

func NewRectangle() Rectangle {
	return Rectangle{
		_type:  "Rectangle",
		height: 2.,
		width:  4.,
	}
}
