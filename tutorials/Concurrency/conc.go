package main

import (
	"fmt"
	"sync"
	"time"
)

func run() error {
	timerFinished := make(chan bool, 1)
	functionsFinished := make(chan bool, 1)
	var err error = nil
	wgTimer := &sync.WaitGroup{}
	var wg = &sync.WaitGroup{}
	amountOfGoRoutines := 10000000
	wgTimer.Add(1)
	var broken = false
	go Timer(wgTimer, timerFinished, functionsFinished)
	for i := 0; i < amountOfGoRoutines; i++ {
		wg.Add(1)
		go concFunc(wg)
		if len(timerFinished) == 1 && <-timerFinished == true {
			err = fmt.Errorf("Time limit exceed")
			broken = true
			break
		}
	}
	if !broken {
		functionsFinished <- true
	}
	if err == nil {
		fmt.Println("Function completed")
	}
	wg.Wait()
	wgTimer.Wait()
	return err
}

func concFunc(wg *sync.WaitGroup) {
	var a int = 3
	var b int = 5
	_ = a + b
	wg.Done()

}

func Timer(wgTimer *sync.WaitGroup, finishedTimer chan bool, finishedFunctions chan bool) {
	timer := time.NewTimer(2 * time.Second)
	<-timer.C
	if len(finishedFunctions) == 0 {
		fmt.Println("2 seconds left")
	}
	finishedTimer <- true
	wgTimer.Done()
}
