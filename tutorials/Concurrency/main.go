package main

import (
	"fmt"
	"sync"
)

func main() {
	task4()
}

func task4() {
	var oddN = make(chan int)
	var evenN = make(chan int)
	var finalArr = [10]int{}
	wg := &sync.WaitGroup{}
	wg.Add(2)
	go even(wg, evenN)
	go odd(wg, oddN)
	for i := 0; i < 5; i++ {
		finalArr[i*2] = <-evenN
		finalArr[i*2+1] = <-oddN
	}
	wg.Wait()
	for i := 0; i < 10; i++ {
		fmt.Printf("%v ", finalArr[i])
	}
}

func task3() {
	runGoroutins()
}

func task2_a() {
	err := newRun()
	if err != nil {
		fmt.Printf("%v", err)
	}
}

func task2() {
	err := run()
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func task1() {
	err := foo()
	fmt.Printf("%v\n", err)
}
