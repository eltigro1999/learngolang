package main

import (
	"fmt"
	"math"
	"math/rand"
)

const PassStatus = "pass"
const FailStatus = "fail"

func fourth_task() {
	var eat string = "съешь ещё этих мягких французских булок, да выпей чаю"
	sym_amount := map[rune]int{}
	for _, value := range eat {
		sym_amount[value]++
	}
	for key, value := range sym_amount {
		fmt.Printf("%v - %v\n", string(key), value)
	}
}

func third_task() bool {
	words := []string{"c", "b", "c", "d"}
	words_length := len(words)
	for i := 1; i < words_length; i++ {
		if words[i-1] > words[i] {
			return false
		}
	}
	return true
}

func second_task() bool {
	nums := make([]int, 4)
	m := make(map[int]int)
	for i := range nums {
		nums[i] = rand.Intn(7)
		m[nums[i]]++
		if m[nums[i]] > 1 {
			return true
		}
	}
	return false
}

func first_task() {

	fmt.Println("Тут будет выведен идентификатор")
	checks := generateCheck()

	for _, value := range checks {
		if value.status == PassStatus {
			fmt.Println(value.ServiceID)
		}
	}
}

func generateCheck() []HealthCheck {
	var checks = make([]HealthCheck, 5)
	for index := range checks {
		checks[index].ServiceID = "2"
		if math.Mod(float64(index), 2) == 0 {
			checks[index].status = PassStatus

		} else {
			checks[index].status = FailStatus
		}
	}
	fmt.Println(checks)

	return checks
}
