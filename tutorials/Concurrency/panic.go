package main

import (
	"fmt"
)

func foo() (err1 error) {

	defer func() error {
		err := recover()

		if err != nil {
			err1 = fmt.Errorf("Error happened: %v", err)
		}
		return err1

	}()
	panic("Panic!!")
}
